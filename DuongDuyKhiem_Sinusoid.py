import numpy as np #thay numpy thanh np
import matplotlib.pyplot as plt #thay matplotlib thanh plt
A = .8 #gan bien do A = 0.8
f = 5 #gan tan so f = 5
t = np.arange(0,1,.01) #tao bien t co gia tri tu 0->1 buoc nhay la 0.01
phi = np.pi/4 #gan goc phi = pi/4
x = A*np.cos(2*np.pi*f*t + phi) #viet bieu thuc cho ham X
plt.plot(t,x) #ve dang song cua x theo t
plt.axis([0,1,-1,1]) #nhan gia tri theo danh sach [xmin, xmax, ymin, ymax] va bieu thi no tren truc so
plt.xlabel('time in seconds') #nhan o truc hoanh
plt.ylabel('amplitude') #nhan o truc tung
plt.show() #hien thi ket qua ve

#=======================================================

A = .65
fs = 100
samples = 100
f = 5
phi = 0
n = np.arange(samples) #tao bien t co gia tri tu 0->100 buoc nhay la 1
T = 1.0/fs #Remember to use 1.0 and not 1!
y = A*np.cos(2*np.pi*f*n*T + phi)
plt.plot(y) #ve do thi cho y
plt.axis([0,100,-1,1]) #nhan gia tri theo danh sach [xmin, xmax, ymin, ymax] va bieu thi no tren truc so
plt.xlabel('sample index') #hien thi nhan tren truc hoanh
plt.ylabel('amplitude') #hien thi nhan tren truc tung
plt.show()

#===============================================================

A = .8
N = 100 # samples
f = 5
phi = 0
n = np.arange(N) #tao bien t co gia tri tu 0->100 buoc nhay la 1
y = A*np.cos(2*np.pi*f*n/N + phi)
plt.plot(y) #ve do thi cho y
plt.axis([0,100,-1,1])#nhan gia tri theo danh sach [xmin, xmax, ymin, ymax] va bieu thi no tren truc
plt.xlabel('sample index')#hien thi nhan tren truc hoanh
plt.ylabel('ampli tude')#hien thi nhan tren truc tung
plt.show()

#============================================================================

f = 3
t = np.arange(0,1,.01)#tao bien t co gia tri tu 0->100 buoc nhay la 0.01
phi = 0
x = np.exp(1j*(2*np.pi*f*t + phi)) #dua x ve dang euler
xim = np.imag(x) #lay phan ao
plt.figure(1) #tao 1 cai khung
plt.plot(t,np.real(x)) #ve phan thuc theo t
plt.plot(t,xim) #ve phan ao theo t
plt.axis([0,1,-1.1,1.1])#nhan gia tri theo danh sach [xmin, xmax, ymin, ymax] va bieu thi no tren truc
plt.xlabel('time in seconds')
plt.ylabel('amplitude')
plt.show()

#=============================================================

f = 3
N = 100
fs = 100
n = np.arange(N)#tao bien t co gia tri tu 0->100 buoc nhay la 1
T = 1.0/fs#tinh chu ky
t = N*T
phi = 0
x = np.exp(1j*(2*np.pi*f*n*T + phi)#dua x ve dang euler
xim = np.imag(x)#lay phan ao
plt.figure(1)#tao 1 cai khung
plt.plot(n*T,np.real(x))#ve phan thuc theo n*T
plt.plot(n*T,xim)#ve phan ao theo n*T
plt.axis([0,t,-1.1,1.1])#nhan gia tri theo danh sach [xmin, xmax, ymin, ymax] va bieu thi no tren truc
plt.xlabel('t(seconds)')#hien thi nhan tren truc hoanh
plt.ylabel('amplitude')#hien thi nhan tren truc tung
plt.show()

#===================================================


f = 3
N = 64
n = np.arange(64)#tao bien n co gia tri tu 0->64 buoc nhay la 1
phi = 0
x = np.exp(1j*(2*np.pi*f*n/N + phi))#dua x ve dang euler
xim = np.imag(x)#lay phan ao
plt.figure(1)#tao mot cai khung
plt.plot(n,np.real(x))#ve phan thuc theo n
plt.plot(n,xim#ve phan ao theo n
plt.axis([0,samples,-1.1,1.1])#nhan gia tri theo danh sach [xmin, xmax, ymin, ymax] va bieu thi no tren truc
plt.xlabel('sample index'#hien thi nhan tren truc hoanh
plt.ylabel('amplitude')#hien thi nhan tren truc tung
plt.show()

#=========================================================

N = 44100 # samples
f = 440
fs = 44100
phi = 0
n = np.arange(N)#tao bien n co gia tri tu 0->N buoc nhay la 1
x = A*np.cos(2*np.pi*f*n/N + phi)#gan x theo dang luong giac
#scipy.io.wavfile.write(filename, rate, data)[source]
from scipy.io.wavfile import write
write('sine440_1sec.wav', 44100, x)
