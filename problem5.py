#===========================================bai 5======================================

dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50, 6:60}

#==============================================

dic4 = {}
for d in (dic1, dic2, dic3):
    dic4.update(d) #update d vao dic4
print(dic4)

#==============================================

d=dict()
for x in range(1,16): #range: tao 1 cai list range([start], stop, [step])
    d[x]=x**2
print(d)

#==============================================

y = {'a':1,'b':4,'c':2}
l = list(y)
l.sort()
print('Ascending order is',l)

#==============================================

str = 'Python is an easy language to learn'
dict = {}
for i in str:
    dict[i] = dict.get(i, 0) + 1
print(dict)

#==============================================