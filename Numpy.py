import numpy as np

#1. mport the numpy package under the name `np` and print out the numpy version and the configuration

print(np.__version__)
np.show_config()

# 2. Create a null vector of size 10

x = np.zeros(10)
print(x)

#3. How to find the memory size of any array
#4. How to get the documentation of the numpy add function from the command line
#5.Create a null vector of size 10 but the fifth value which is 1

x = np.zeros(10)
x[4] = 1
print(x)

#6. Create a vector with values ranging from 10 to 49

x = np.arange(10,50)
print(x)

#7. Reverse a vector (first element becomes last)

x = np.arange(50)
x = x[::-1]
print(x)
#print(list(reversed(x)))

#8. Create a 3x3 matrix with values ranging from 0 to 8

x = np.arange(9).reshape(3,3)
print(x)

#9. Find indices of non-zero elements from \\[1,2,0,0,4,0\\]

x = np.nonzero([1,2,0,0,4,0])
print(x)

#10. Create a 5x6 zero matrix

x = np.zeros(30).reshape(5,6)
print(x)

#11. Create a 3x3 identity matrix

x = np.eye(3)
print(x)

#12. Create a 3x3x3 array with random values

x = np.random.random((3,3,3))
print(x)

#13. Create a 10x10 array with random values and find the minimum and maximum values

x = np.random.random((10,10))
xmin = x.min()
xmax =  x.max()
print("xmin, xmax = ",xmin, xmax)

#14. Create a random vector of size 30 and find the mean value//diem trung tam: tong/len

x = np.random.random(30)
m = x.mean()
print(m)

#15. Create a 2d array with 1 on the border and 0 inside

x = np.ones((10,10))
x[1:-1,1:-1] = 0
print(x)

#16. How to add a border (filled with 0's) around an existing array?
#17. What is the result of the following expression?
#18. Create a 5x5 matrix with values 1,2,3,4 just below the diagonal
#19. Create a 8x8 matrix and fill it with a checkerboard pattenr
#20. Consider a (6,7,8) shape array, what is the index (x,y,z) of the 100th element
#21. Create a checkerboard 8x8 matrix using the tile function
#22. Normalize a 5x5 random matrix
#23. Create a custom dtype that describes a color as four unsigned bytes (RGBA)
#24. Multiply a 5x3 matrix by a 3x2 matrix (real matrix product)
#25. Alternative solution, in Python 3.5 and above
#26. Consider an integer vector Z, which of these expressions are legal
#27. How to round away from zero a float array
#28. How to find common values between two arrays

x1 = np.random.randint(0,9,9)
x2 = np.random.randint(0,9,9)
print(np.intersect1d(x1,x2))

#29.  How to ignore all numpy warnings (not recommended)
#30. How to get the dates of yesterday, today and tomorrow?

yesterday = np.datetime64('today', 'D') - np.timedelta64(1, 'D')
print(yesterday)
today = np.datetime64('today', 'D')
print(today)
tomorrow = np.datetime64('today', 'D') + np.timedelta64(1, 'D')
print(tomorrow)