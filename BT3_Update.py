from functools import reduce
#=============================== bai 1 =================================
#1.	Given the following list l1
#a.	Write a recursive function named sum_deep()
# that get l1 as an argument and recursively calculate the sum of numbers in this list.

l4 = [3, 4, "hello", (4.6, 5.6), [3,7,2,2, [0.3, 5],"text"], 6, 3, 3.5];
l5 = [];
def sum_def (temp):
    for i in temp:
        if(type(i) != tuple and type(i) != list and type(i) != dict ):
            l5.append(i);
        else:
            sum_def(i);

sum_def(l4);

sum = 0;
for i in l5:
    if(type(i) != str):
        sum = sum + i;
print("tinh tong su dung de qui la: ", sum);

#=============================== bai 2 =================================

# 2.	Write a function that return a list of  max value of two lists as follows:
# l1 = [2,4,6,4,9,4,7]
# l2 = [5,3,5,6,9,7,5]
# max_of_pair(l1,l2) => [5,4,6,6,9,7,7]
# NOTE: check if l1 or l2 exists non-number

def max_of_pair(l1,l2) :
    i = 0
    l3 = []
    if (len(l1) == 0) or (len(l2) == 0):
        print("list is empty!")
        return
    while i < len(l1):
        if (l1[i] < l2[i]) or (l1[i] == l2[i]):
            l3.append(l2[i])
        else:
            l3.append(l1[i])
        i+=1
    return l3

l1 = [2,4,6,4,9,4,7]
l2 = [5,3,5,6,9,7,5]
print("max_of_pair(l1,l2): " ,max_of_pair(l1,l2));

#==================================== bai 3 ==================================

# 3.	Given a word_count_data.txt (shared folder)
# write a function to count word of this file using map and reduce functions.

listSum = [];

#tra ve list gom tong cac tu cua tung chi muc trong txt ban dau
def dem(str):
    sum = 0;
    #tach chuoi thanh tung phan tu trong list va dem so tu o tung chi muc
    sum = sum + len(str.split());
    #them tong tu vao list moi
    listSum.append(sum);
    #return listSum;

#tinh tong cac phan tu trong list
def tinhTong(listTemp):
    tong = 0;
    for i in listTemp:
        tong = tong + i;
    return tong;

def count_Word_Text(tongWord, num):
    return tongWord  + num;

#==================================== map ======================================
f = open("D:/DuongDuyKhiem_N16DCDT049_N16CQDT01-N_13Oct2016/tuHocPython/project_xltn/baiTapBuoi3/Day 3 -28-8-19/word_count_data.txt", "r");
line=f.readlines();

result=(map(dem, line));
list(result);
print("tong chu so trong .txt dung ham map() la: ", tinhTong(listSum));

f.close();
#==================================== reduce ======================================

f = open("D:/DuongDuyKhiem_N16DCDT049_N16CQDT01-N_13Oct2016/tuHocPython/project_xltn/baiTapBuoi3/Day 3 -28-8-19/word_count_data.txt", "r");
line=f.readlines();

print("tong chu so trong .txt dung ham reduce() la:", reduce(count_Word_Text, listSum));

f.close();

#========================print====================
def print_iterator(it):
    for x in it:
        print(x, end=' ');
    print(''); # for new line